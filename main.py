from scraper import Scraper
import json

if __name__ == "__main__":
  scraper = Scraper()

  datas = scraper.get_data()
  index = 1
  #print(datas)
  f = open("kopi.json","w")
  j = json.dumps(datas)
  f.write(j)
  f.close()
  for data in datas:
    print(
      index,
      data['name'],
      data['img'],
      data['price'],
      data['city']
    )

    index += 1